import 'package:dioc/dioc.dart' as di;
import 'package:fluttertest/application/GetCounter.dart';
import 'package:fluttertest/domain/CounterRepository.dart';
import 'package:fluttertest/infrastructure/InMemoryCounterRepository.dart';
import 'package:fluttertest/presentation/ApplicationRootWidget.dart';

var container = new di.Container();

void initContainer() {
  container.register(CounterRepository, (c) => new InMemoryCounterRepository());
  container.register(GetCounter, (c) => new GetCounterImpl(c.singleton(CounterRepository)));
  container.register(ApplicationRootWidget, (c) => new ApplicationRootWidget(c.singleton(GetCounter)));
}
