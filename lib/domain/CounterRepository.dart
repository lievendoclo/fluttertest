import 'package:fluttertest/domain/Counter.dart';

abstract class CounterRepository {
  Counter createNew();
}