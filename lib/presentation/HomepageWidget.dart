import 'package:flutter/material.dart';
import 'package:fluttertest/application/GetCounter.dart';
import 'package:fluttertest/domain/Counter.dart';

class HomepageWidget extends StatefulWidget {
  HomepageWidget(this._getCounter, {Key key, this.title}) : super(key: key);

  final String title;
  final GetCounter _getCounter;

  @override
  HomepageWidgetState createState() => new HomepageWidgetState(_getCounter.getCounter());
}

class HomepageWidgetState extends State<HomepageWidget> {
  Counter counter;

  HomepageWidgetState(this.counter);

  void _incrementCounter() {
    setState(() {
      counter.increment();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text(
              'You have pushed this button this many times:',
            ),
            new Text(
              '${counter.count}',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: new Icon(Icons.add),
      ), 
    );
  }
}
