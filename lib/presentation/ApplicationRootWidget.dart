import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertest/application/GetCounter.dart';
import 'package:fluttertest/presentation/HomepageWidget.dart';

class ApplicationRootWidget extends StatelessWidget {
  final GetCounter _getCounter;

  ApplicationRootWidget(this._getCounter);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.green,
      ),
      home: new HomepageWidget(_getCounter, title: 'Flutter Demo Home Page')
      );
  }
}