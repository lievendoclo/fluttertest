import 'package:flutter/material.dart';

import 'package:fluttertest/presentation/ApplicationRootWidget.dart';
import 'package:fluttertest/DiContainer.dart' as di;

void main() {  
  di.initContainer();
  runApp(di.container.singleton(ApplicationRootWidget));
}

