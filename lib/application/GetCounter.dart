import 'package:fluttertest/domain/Counter.dart';
import 'package:fluttertest/domain/CounterRepository.dart';

abstract class GetCounter {
  Counter getCounter();
}

class GetCounterImpl implements GetCounter {
  CounterRepository _counterRepository;

  GetCounterImpl(this._counterRepository);

  @override
  Counter getCounter() {
    return _counterRepository.createNew();
  }
}