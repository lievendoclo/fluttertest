import 'package:fluttertest/domain/Counter.dart';
import 'package:fluttertest/domain/CounterRepository.dart';

class InMemoryCounterRepository implements CounterRepository {
  @override
  Counter createNew() {
    return new Counter();
  }
}